import { Link } from "react-router-dom";

import '../../pages/home.css';

export default function ListMovies({movies}) {
    return (
        <div className="listMovies">

            {movies === "Nenhum filme encontrado." ? <p className="erroList">Nenhum filme encontrado.</p> : movies.map((movie) => (
                <div className="listInfo" key={movie.id}>
                    <img src={movie.image} className="listImage" width="150" height="150"></img>
                    <div className="infoTitleButton">
                        <p className="listTitle">{movie.title}</p>
                        <p className="listDescription">{movie.description}</p>
                        <Link className="listButton" to={"/movie/" + movie.id + "/" + movie.title} target="_blank">Saiba mais</Link>
                    </div>
                </div>
            ))}
        </div>
    )
}