import {useState} from 'react';

import { Search } from '../../domain/search';
import ListMovies from './list';
import Loader from "../loader/loader";
import '../../pages/home.css';

export default function Form() {
    const [text, setText] = useState("");
    const [allMovies, setAllMovies] = useState([])
    const [returnText, setReturnText] = useState("Digite nome do filme");
    const [load, setLoad] = useState(false);

    const searchMovie = async (text: String): Promise<void> => {
        setLoad(true);
        let list = await Search.searchList(text);
        setLoad(false);

        setAllMovies(list);
        return allMovies;
    } 

    return (
        <div role='form' className='formSearch'>
            
            <input className='formNameMovie' type="text" placeholder="Digite o nome do filme" onChange={(e): void => setText(e.target.value)} required />
            <button className='formButtonSearch' onClick={(): Promise<void> => searchMovie(text)} disabled={!text}>Buscar</button>
         
            {load ? <Loader /> : <ListMovies movies={allMovies}/>}
        </div>
    )
}
