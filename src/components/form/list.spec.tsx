import ListMovies from './list';

describe('List component', () => {    
    it('List Movies', () => {
        let newMovies: {id: string; resultType: string; image: string; title: string; description: string;} = {id: 'tt0830515', resultType: 'Title', image: 'image.jpg', title:'Quantum of Solace', description: '007 Quantum of Solace'};

        let addMovies: any = ListMovies({newMovies});
        let expectNewTask: [{id: string; resultType: string; image: string; title: string; description: string;}] = [{id: 'tt0830515', resultType: 'Title', image: 'image.jpg', title:'Quantum of Solace', description: '007 Quantum of Solace'}]
        expect(expectNewTask).toEqual(expect.arrayContaining(addMovies));
    })
});
