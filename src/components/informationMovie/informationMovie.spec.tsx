import informationMovie from './informationMovie';

describe('Information Movie component', () => {    
    it('Info Movie', () => {
        let info: {fullTitle: string; year: string; plot: string; actorList: Object;} = {fullTitle: "The Bond Essentials (2002)", year: "2002", plot: "007", actorList: [{id: 'nm0002184', image: 'image.jpg', name: 'Vic Armstrong', asCharacter: 'Selfas Self'}]};
        let addInfoMovies: any = informationMovie({info});
        let expectNewTask: [{fullTitle: string; year: string; plot: string; actorList: Object;}] = [{fullTitle: "The Bond Essentials (2002)", year: "2002", plot: "007", actorList: [{id: 'nm0002184', image: 'image.jpg', name: 'Vic Armstrong', asCharacter: 'Selfas Self'}]}]
        expect(expectNewTask).toEqual(expect.arrayContaining(addInfoMovies));
    })
});
