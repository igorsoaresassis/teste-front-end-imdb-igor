import react, { Component, useState } from "react";
import { useParams } from "react-router-dom";

import { Information } from '../../domain/information';
import InformationMovie from './informationMovie';
import Loader from "../loader/loader";
import '../../pages/home.css';

export default function AboutMovie() {
    const { id, title } = useParams();
    const [Movie, setMovie] = useState([])
    const [load, setLoad] = useState(false);

    const data = async (id: any, title: any): Promise<void> => {
        setLoad(true);
        let infoMovie = [];
        let movie = await Information.information(id, title);

        infoMovie.push(movie)
        setMovie(infoMovie[0]);
        
        setLoad(false);
        return Movie;
    }

    window.onload = function exampleFunction() {
        data(id, title);
    }

    return (
        <div className="infoMovie">
            {load ? <Loader /> : <InformationMovie movie={Movie} />}
        </div>
    )
}
