import React, { useRef, useState } from 'react'
import '../../pages/home.css';

export default function InformationMovie({movie}) {
    return (
        <div className="informationMovie">
            <img src={movie.image} className="informationMovieImage" width="250" height="250"></img>
            <p className="informationMovieTitle">{movie.fullTitle}</p>
            <p className="informationMovieYear">Ano: {movie.year}</p>
            <p className="informationMoviePlot">{movie.plot}</p>
            <div className="actorInfo">
                {(movie.actorList ?? []).map((info) => (
                    <div className='info' key={info.id}>
                        <img src={info.image} className="informationMovieImage" width="100" height="100"></img>
                        <p className="informationMovieName">{info.name}</p>
                        <p className="informationMovieCharacter">{info.asCharacter}</p>
                    </div>
                ))}
            </div>
        </div>
    )
}
