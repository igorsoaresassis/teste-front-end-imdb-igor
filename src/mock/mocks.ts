export default [
  {
    id: 'tt0830515', 
    resultType: 'Title', 
    image: 'https://imdb-api.com/images/original/MV5BMzhjYzlmY…yXkFqcGdeQXVyMjUzOTY1NTc@._V1_Ratio0.7273_AL_.jpg', 
    title: 'Quantum of Solace', 
    description: '(2008) aka "007 Quantum of Solace"'
  }
];
