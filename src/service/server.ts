import React from "react";

export class Server {
    static API_URL_Search = `https://imdb-api.com/en/API/SearchMovie`;
    static API_URL_Title = `https://imdb-api.com/en/API/Title`;
}
