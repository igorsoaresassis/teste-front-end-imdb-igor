import React, {Component} from 'react';
import { Route, Switch } from 'react-router-dom';

import FormSearch from '../components/form/form';
import AboutMovie from '../components/informationMovie/aboutMovie';

import logo from '../assets/logo.svg';
import './home.css';

export default class Home extends Component {
    
    render() {
        return ( 
            <Switch>
                <Route path='/movie/:id/:title'>
                    <AboutMovie />
                </Route>
                <Route path='/'>
                    <div className='home'>
                        <div className='title-logo'>
                            <img src={logo} className="logo" alt="logo" />
                            <h2 className='title'>Pottencial Serguros</h2>
                        </div>
                        <FormSearch />
                    </div>
                </Route>
            </Switch>
        )
    }

}
