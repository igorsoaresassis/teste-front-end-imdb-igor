import axios from 'axios';

import { Server } from './../service/server';

export class Search {

  static searchList(name: String) {
        const apiKey = 'k_w15t50je';
        const api = Server.API_URL_Search + "/" + apiKey + "/" + name;

        return axios
          .get(api)
          .then(data => {

            if(data.data.results.length === 0) {
              return "Nenhum filme encontrado.";
            }
            return data.data.results;
          });
    }
}


