import axios from 'axios';

import { Server } from './../service/server';

export class Information {

  static information(id: String, name: String) {
        const apiKey = 'k_w15t50je';
        const api = Server.API_URL_Title + "/" + apiKey + "/" + id + "/" + name;

        return axios
          .get(api)
          .then(data => {
            return data.data;
          });
    }
}


