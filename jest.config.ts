export default {
  clearMocks: true,
  collectCoverage: true,
  coverageDirectory: 'coverage',
  coverageProvider: 'v8',
  testMatch: ['**/src/**/**/*.spec.ts'],
  preset: 'ts-jest',
}
