# Teste front end

### 🛠 Tecnologias

O Projeto foi feito de acordo com as instruções abaixo, não fui utilizado nenhuma biblioteca de UI e os testes foram feitos em Jest. 

### 🎲 Excutando a aplicação
```bash
# Clone este repositório dentro da página que deseja salvar
$ git clone https://gitlab.com/igorsoaresassis/teste-front-end-imdb-igor.git

# Instale as dependências
$ npm install

# Execute a aplicação
$ npm run dev

# A aplicação inciará na porta:3000 - acesse <http://localhost:3000>

# Para executar os testes
$ npm run test
```

## Sobre o teste

A aplicação utilizará a API do imdb pra permitir:

- A busca de filmes por uma palavra chave
- Os filmes retornados pela API devem ser listados
- Ao clicar em um filme, deve ser navegado para uma página que exibe mais informações do filme

## Funcionalidades

Na tela inicial, deve ser exibido uma barra de busca grande onde será digitado um nome do filme pelo usuário.

Ao faze a busca, deve ser exibido logo a baixo da barra de busca a listagem com os filmes encontrados, exibindo as seguintes informações:

- Nome do filme
- Descrição do filme
- Imagem da capa do filme

Ao clicar em um filme, o usuário deve ser redirecionado para uma página onde deve ser exibido mais informações sobre o filme selecionado:

- Ano de publicação do filme
- Titulo do filme
- Descrição do filme
- O enredo do filme (plot)
- Lista de atores, contendo:
  - Nome do ator
  - Imagem do ator
  - Nome do persoangem que interpretou no filme
  
Deve ser possível compartilhar a url de listagem do filme assim como a url da página de exibição dos detalhados de um filme. De forma que a url compartilhada exiba exatamente as mesmas informações quando acessada.

## Requisitos básicos

- Crie um fork deste projeto (https://gitlab.com/Pottencial/teste-front-end-imdb/-/forks/new). É preciso estar logado na sua conta Gitlab
- Adicione @Pottencial (Pottencial Seguradora) como membro do seu fork. Você pode fazer isto em  https://gitlab.com/`your-user`/tech-test-frontend-todo/settings/members
- Utilzar React
- Realizar testes unitários, preferencialmente com jest e react-testing-library. Mas pode utilizar outros frameworks caso prefira
- A aplicação não deve utilizar libs css e/ou javascript diferentes de:
  - react
  - react-dom
  - react-router
- O projeto deve possuir um README com instruções básicas de como rodar a aplicação

Para testes e setup do projeto pode ser utilizado quais libs quiser.

## Requisitos adicionais

- Aqui nós costumamos utilizar redux, e mesmo que essa aplicação seja pequena, seria bom se fosse utilizado para demonstrar seus conhecimentos.
- As páginas devem ficar responsívas, se ajustando aos diferentes tamanhos de tela de forma que nenhum elemento fique cortado
- Nos preocupamos com a tratativa de caminhos alternativos na experiência do usuário, por tanto, avaliaremos por exemplo o comportamento da aplicação caso a busca não retorne um filme, e caso o id do filme presente na url de detalhes do filme não exista.

## API

Deve ser utilizado a API do imdb. Você pode criar sua conta, e ver como utilizar a API no site da [API do IMDb](https://imdb-api.com/api).

Para o problema descrito, deve ser utilizado as rotas:

- https://imdb-api.com/en/API/SearchMovie: Para consultar filmes por uma palavra chave
- https://imdb-api.com/en/API/Title: Para obter mais informações de um filme